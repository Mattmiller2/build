This is the readme for the coursework 2 build.

This is a database system that allows for the creation and management of questions and questionnaires. There is also a front end for participants to answer questionnaires and submit them to the Admin back end. Then the responses to the questionnaires can be viewed in the back end. 

As well as this there the back end is secure and requires logging in and the correct credentials to access the back end features.
