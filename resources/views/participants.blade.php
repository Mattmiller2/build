<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Available Questionnaires</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
<h1>Available Questionnaires</h1>

<p>Please click on one of the questionnaires below to begin the questionnaire.</p>

<!--Shows all created questionnaires available to the user-->
<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
                <li><a href="/participants/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p>
    @endif
</section>
</body>
</html>
