<!--Admin Exclusive navigation bar -->
<nav>
  <ul>
    <li><a href="/admin/questions">Create a new question</a></li>
    <li><a href="/admin/questionnaires">Create a new questionnaire</a></li>
    <li><a href="/admin/responses">See all responses</a></li>
  </ul>
</nav>
