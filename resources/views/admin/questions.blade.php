<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questions</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
  <!--Requires permission to show the adminnav-->
  @can('see_adminnav')

    @include('admin/includes/adminnav')

  @endcan

<h1>Questions</h1>

<p> All Created Questions</p>

<!--Shows all created questions as hyperlinks-->
<section>
    @if (isset ($questions))

        <ul>
            @foreach ($questions as $question)
                <li><a href="/admin/questions/{{ $question->id }}" name="{{ $question->title }}">{{ $question->content }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questions added yet </p>
    @endif
</section>

<!--Directs user to the create form-->
{{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}
    <div class="row">
      {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

</body>
</html>
