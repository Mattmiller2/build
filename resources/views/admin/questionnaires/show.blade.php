<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $questionnaire->title }}</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
<h1>{{ $questionnaire->title }}</h1>

<!--Shows the Questions that have been linked in a list-->
<section>
<p>Questions:</p>

@if (isset ($questionnaire->question))
  <ol>
    @foreach ($questionnaire->question as $question)
     <li><p>{{ $question->content }}</p></li>
     @endforeach
   </ol>
</section>
@else
  <p>No Questions linked in</p>
@endif

<!--Shows the answers related to the question-->
<p>Question 1:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q1_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q1_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q1_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q1_answerD }}</p></li>
</ul>

<!--Shows the answers related to the question-->
<p>Question 2:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q2_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q2_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q2_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q2_answerD }}</p></li>
</ul>

<!--Shows the answers related to the question-->
<p>Question 3:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q3_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q3_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q3_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q3_answerD }}</p></li>
</ul>

<!--Shows the answers related to the question-->
<p>Question 4:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q4_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q4_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q4_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q4_answerD }}</p></li>
</ul>

<!--Shows the answers related to the question-->
<p>Question 5:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q5_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q5_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q5_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q5_answerD }}</p></li>
</ul>

<!--Returns the user to the questionnaire blade-->
{{ Form::open(array('action' => 'QuestionnaireController@index', 'method' => 'get')) }}
    <div class="row">
      {!! Form::submit('Back', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}
</body>
</html>
