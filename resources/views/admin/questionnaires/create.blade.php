<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Questionnaire</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
<h1>Add Questionnaire</h1>


{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
        {{ csrf_field() }}
    <!--Allows for entry of the questionnaire name -->
    <div class="row large-12 columns">
        {!! Form::label('title', 'Enter Questionnaire Name:') !!}
        {!! Form::text('title', null, ['class' => 'large-12 columns']) !!}
    </div>

    <!--Allows for entry of the questions -->
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question 1:') !!}
        {!! Form::select('question[]', $questions, null,['class' => 'large-12 columns', 'multiple']) !!}
    </div>

    <!--Allows for entry of the answer A to D-->
    <div class="row large-12 columns">
        {!! Form::label('Q1_answerA', 'Answer A:') !!}
        {!! Form::text('Q1_answerA', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q1_answerB', 'Answer B:') !!}
        {!! Form::text('Q1_answerB', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q1_answerC', 'Answer C:') !!}
        {!! Form::text('Q1_answerC', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q1_answerD', 'Answer D:') !!}
        {!! Form::text('Q1_answerD', null, ['class' => 'large-8 columns']) !!}
    </div>

    <!--Allows for entry of the question -->
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question 2:') !!}
        {!! Form::select('question[]', $questions, null,['class' => 'large-12 columns', 'multiple']) !!}
    </div>

    <!--Allows for entry of the answer A to D-->
    <div class="row large-12 columns">
        {!! Form::label('Q2_answerA', 'Answer A:') !!}
        {!! Form::text('Q2_answerA', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q2_answerB', 'Answer B:') !!}
        {!! Form::text('Q2_answerB', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q2_answerC', 'Answer C:') !!}
        {!! Form::text('Q2_answerC', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q2_answerD', 'Answer D:') !!}
        {!! Form::text('Q2_answerD', null, ['class' => 'large-8 columns']) !!}
    </div>

    <!--Allows for entry of the question -->
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question 3:') !!}
        {!! Form::select('question[]', $questions, null,['class' => 'large-12 columns', 'multiple']) !!}
    </div>

    <!--Allows for entry of the answer A to D-->
    <div class="row large-12 columns">
        {!! Form::label('Q3_answerA', 'Answer A:') !!}
        {!! Form::text('Q3_answerA', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q3_answerB', 'Answer B:') !!}
        {!! Form::text('Q3_answerB', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q3_answerC', 'Answer C:') !!}
        {!! Form::text('Q3_answerC', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q3_answerD', 'Answer D:') !!}
        {!! Form::text('Q3_answerD', null, ['class' => 'large-8 columns']) !!}
    </div>

    <!--Allows for entry of the question -->
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question 4:') !!}
        {!! Form::select('question[]', $questions, null,['class' => 'large-12 columns', 'multiple']) !!}
    </div>

    <!--Allows for entry of the answer A to D-->
    <div class="row large-12 columns">
        {!! Form::label('Q4_answerA', 'Answer A:') !!}
        {!! Form::text('Q4_answerA', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q4_answerB', 'Answer B:') !!}
        {!! Form::text('Q4_answerB', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q4_answerC', 'Answer C:') !!}
        {!! Form::text('Q4_answerC', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q4_answerD', 'Answer D:') !!}
        {!! Form::text('Q4_answerD', null, ['class' => 'large-8 columns']) !!}
    </div>

    <!--Allows for entry of the question -->
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question 5:') !!}
        {!! Form::select('question[]', $questions, null,['class' => 'large-12 columns', 'multiple']) !!}
    </div>

    <!--Allows for entry of the answer A to D-->
    <div class="row large-12 columns">
        {!! Form::label('Q5_answerA', 'Answer A:') !!}
        {!! Form::text('Q5_answerA', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q5_answerB', 'Answer B:') !!}
        {!! Form::text('Q5_answerB', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q5_answerC', 'Answer C:') !!}
        {!! Form::text('Q5_answerC', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div class="row large-12 columns">
        {!! Form::label('Q5_answerD', 'Answer D:') !!}
        {!! Form::text('Q5_answerD', null, ['class' => 'large-8 columns']) !!}
    </div>

    <!--Submits the questionnaire-->
    <div class="row large-4 columns">
        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}

</body>
</html>
