<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $question->title }}</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
<h1>{{ $question->title }}</h1>
<p>{{ $question->content }}</p>

<!--Returns the user to the question blade-->
{{ Form::open(array('action' => 'QuestionController@index', 'method' => 'get')) }}
    <div class="row">
      {!! Form::submit('Back', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}
</body>
</html>
