<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Question</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
<h1>Add Question</h1>

{!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
        {{ csrf_field() }}

    <!--Allows entry for question title-->
    <div class="row large-12 columns">
        {!! Form::label('title', 'Question Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <!--Allows entry for the question content-->
    <div class="row large-12 columns">
        {!! Form::label('content', 'Question:') !!}
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <!--Submits the question to the table-->
    <div class="row large-4 columns">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
{!! Form::close() !!}

</body>
</html>
