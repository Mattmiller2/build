<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $response->title }}</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
<h1>({{ $response->id }}) {{ $response->title }}</h1>

<!--shows the responses of the questionnaires-->
@if (isset ($response->answer))
  <ul>
    @foreach ($response->answer as $answer)
      <li><p>{{ $answer->detail }}</p></li>
    @endforeach
  </ul>
@else
  <p>No Answers linked in</p>
@endif

<!--Returns the user to the responses blade-->
{{ Form::open(array('action' => 'ResponseController@index', 'method' => 'get')) }}
    <div class="row">
      {!! Form::submit('Back', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

</body>
