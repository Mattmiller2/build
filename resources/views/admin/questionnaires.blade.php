<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questionnaires</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
  <!--Requires permission to show the adminnav-->
  @can('see_adminnav')

    @include('admin/includes/adminnav')

  @endcan

<h1>Questionnaires</h1>

<p>All created questionnaires</p>

<!--Shows all created questionaires as hyperlinks-->
<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
                <li><a href="/admin/questionnaires/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p>
    @endif
</section>

<!--Directs user to the create form-->
{{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
    <div class="row">
      {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

</body>
</html>
