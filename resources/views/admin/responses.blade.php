<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Responses</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>

<!--Requires permission to show the adminnav-->
  @can('see_adminnav')

    @include('admin/includes/adminnav')

  @endcan

<h1>Responses</h1>

<p> All responses to the questionnaires</p>

<!--Shows all created responses as hyperlinks-->
<section>
    @if (isset ($responses))

        <ul>
            @foreach ($responses as $response)
                <li><a href="/admin/responses/{{ $response->id }}" name="{{ $response->title }}">{{ $response->title }}({{ $response->id }})</a></li>
            @endforeach
        </ul>
    @else
        <p> no Responses added yet </p>
    @endif
</section>
</body>
</html>
