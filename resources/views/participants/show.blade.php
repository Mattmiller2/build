<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $questionnaire->title }}</title>
</head>
<link rel="stylesheet" href="/css/app.css" />
<body>
<h1>{{ $questionnaire->title }}</h1>

<!--Shows all questions for the questionnaire-->
<section>
<p>Questions:</p>

@if (isset ($questionnaire->question))
  <ol>
    @foreach ($questionnaire->question as $question)
    <li><p>{{ $question->content }}</p></li>
     @endforeach
   </ol>
</section>
@else
  <p>No Questions linked in</p>
@endif

<!--Ethics statement-->
<div class="row lage-12 columns">
  <sub>Please Read this before starting the questionnaire. </sub>
  <sub>All data that is collected from your submission to this questionnaire will be kept 100 percent anonymous and will not be shared with third party organisations</sub>
  <sub>As well as this you have the right to withdraw from this questionnaire before submission whenver you choose. If you decide that you would like to leave. Simply click the exit button below.</sub>
  <sub>Thank you for your time</sub>
</div>

<!--Allows user to exit the form and delete all of the input-->
{{ Form::open(array('action' => 'ParticipantController@index', 'method' => 'get')) }}
    <div class="row">
      {!! Form::submit('Exit Questionnaire', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

{!! Form::open(array('action' => 'ParticipantController@store', 'id' => 'createquestionnaire')) !!}
        {{ csrf_field() }}

<!--Allows entry for the questionnaire title-->
<div class="row large-12 columns">
      {!! Form::label('title', 'Please Enter Questionnaire name:') !!}
      {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
  <p> These are the Answers to the Questions above.</p>
</div>

<!--Shows all created answers for the question-->
<div class="row large-12 columns">
<p>Question 1 Answers:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q1_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q1_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q1_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q1_answerD }}</p></li>
</ul>
</div>

<!--Allows the user to select an answer-->
<div class="row large-12 columns">
    {!! Form::label('answer', 'Question 1 Answer:') !!}
    {!! Form::select('answer[]', $answers, null,['class' => 'large-2 columns', 'multiple']) !!}
</div>

<!--Shows all created answers for the question-->
<div class="row large-12 columns">
<p>Question 2 Answers:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q2_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q2_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q2_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q2_answerD }}</p></li>
</ul>
</div>

<!--Allows the user to select an answer-->
<div class="row large-12 columns">
    {!! Form::label('answer', 'Question 2 Answer:') !!}
    {!! Form::select('answer[]', $answers, null,['class' => 'large-2 columns', 'multiple']) !!}
</div>

<!--Shows all created answers for the question-->
<div class="row large-12 columns">
<p>Question 3 Answers:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q3_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q3_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q3_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q3_answerD }}</p></li>
</ul>
</div>

<!--Allows the user to select an answer-->
<div class="row large-12 columns">
    {!! Form::label('answer', 'Question 3 Answer:') !!}
    {!! Form::select('answer[]', $answers, null,['class' => 'large-2 columns', 'multiple']) !!}
</div>

<!--Shows all created answers for the question-->
<div class="row large-12 columns">
<p>Question 4 Answers:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q4_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q4_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q4_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q4_answerD }}</p></li>
</ul>
</div>

<!--Allows the user to select an answer-->
<div class="row large-12 columns">
    {!! Form::label('answer', 'Question 4 Answer:') !!}
    {!! Form::select('answer[]', $answers, null,['class' => 'large-2 columns', 'multiple']) !!}
</div>

<!--Shows all created answers for the question-->
<div class="row large-12 columns">
<p>Question 5 Answers:</p>
<ul>
  <li><p>A: {{ $questionnaire->Q5_answerA }}</p></li>
  <li><p>B: {{ $questionnaire->Q5_answerB }}</p></li>
  <li><p>C: {{ $questionnaire->Q5_answerC }}</p></li>
  <li><p>D: {{ $questionnaire->Q5_answerD }}</p></li>
</ul>
</div>

<!--Allows the user to select an answer-->
<div class="row large-12 columns">
    {!! Form::label('answer', 'Question 5 Answer:') !!}
    {!! Form::select('answer[]', $answers, null,['class' => 'large-2 columns', 'multiple']) !!}
</div>

<div class="row large-12 columns">
<p> When you have selected your answers then click "Submit".</p>
</div>

<!--Submits the response-->
<div class="row large-4 columns">
    {!! Form::submit('Submit', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}


</body>
</html>
