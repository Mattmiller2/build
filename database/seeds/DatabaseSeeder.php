<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Answer;
use App\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Unguards the model
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Model::unguard();

        //Deletes data in the tables
        Answer::truncate();
        User::truncate();
        Permission::truncate();

        //reguards the models
        Model::reguard();

        //Sets the data value to 1
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        //Runs the seeders to put data in the tables/
        $this->call(AnswersTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);

        //Creates 50 fake users
        factory(User::class, 50)->create();
    }
}
