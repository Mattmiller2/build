<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //Creates the permissions in the permissions table
      DB::table('permissions')->insert([
        ['id' => 1, 'name' => "see_adminnav", 'label' => "Can see the adminnav"],
        ['id' => 2, 'name' => "create_question", 'label' => "Can create a question"],
        ['id' => 3, 'name' => "create_questionnaire", 'label' => "Can create a questionnaire"],
        ['id' => 4, 'name' => "see_responses", 'label' => "Can see responses"],
      ]);
    }
}
