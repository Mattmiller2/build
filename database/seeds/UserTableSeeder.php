<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //Creates a personal user for the creator
        DB::table('users')->insert([
          ['id' => 1, 'name' => "Matthew Miller",
              'email' => 'matthew@example.com',
              'password' => bcrypt('password'),
              'remember_token' => str_random(10),
            ],
        ]);

    }
}
