<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //Creates the data for the answers table
        DB::table('answers')->insert([
          ['id' => 1, 'title' => "a", 'detail' => "A"],
          ['id' => 2, 'title' => "b", 'detail' => "B"],
          ['id' => 3, 'title' => "c", 'detail' => "C"],
          ['id' => 4, 'title' => "d", 'detail' => "D"],
        ]);
    }
}
