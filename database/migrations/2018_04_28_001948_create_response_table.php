<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Create the responses table
        Schema::create('responses', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //Delete the responses table
        Schema::drop('responses');
    }
}
