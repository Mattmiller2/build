<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //create the answer response pivot table
        Schema::create('answer_response', function (Blueprint $table) {
          $table->integer('answer_id');
          $table->integer('response_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //Delete tthe answer response pivot table
        Schema::drop('answer_response');
    }
}
