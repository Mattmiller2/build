<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Creates Answers table
        Schema::create('answers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title')->unique();
          $table->text('detail');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //Deletes answers table
        Schema::drop('answers');
    }
}
