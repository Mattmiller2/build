<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Creates Questionnaire table and columns
        Schema::create('questionnaires', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title')->unique();
          $table->string('Q1_answerA');
          $table->string('Q1_answerB');
          $table->string('Q1_answerC');
          $table->string('Q1_answerD');
          $table->string('Q2_answerA');
          $table->string('Q2_answerB');
          $table->string('Q2_answerC');
          $table->string('Q2_answerD');
          $table->string('Q3_answerA');
          $table->string('Q3_answerB');
          $table->string('Q3_answerC');
          $table->string('Q3_answerD');
          $table->string('Q4_answerA');
          $table->string('Q4_answerB');
          $table->string('Q4_answerC');
          $table->string('Q4_answerD');
          $table->string('Q5_answerA');
          $table->string('Q5_answerB');
          $table->string('Q5_answerC');
          $table->string('Q5_answerD');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //Deletes Questionnaire table
        Schema::drop('questionnaires');
    }
}
