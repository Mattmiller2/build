<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Creates the role user table
        Schema::create('role_user', function(Blueprint $table) {
          $table->integer('role_id')->unsigned();
          $table->integer('user_id')->unsigned();

          $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

          $table->primary(['role_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //Deletes the role user table 
        Schema::drop('role_user');
    }
}
