<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Create the question questionnaire pivot table
        Schema::create('question_questionnaire', function(Blueprint $table) {
          $table->integer('question_id');
          $table->integer('questionnaire_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //Delete the question questionanire pivot table
        Schema::drop('question_questionnaire');
    }
}
