<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Creates Questions table
      Schema::create('questions', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title')->unique();
        $table->text('content');
        $table->integer('author_id')->unsigned()->default(0);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //Deletes Questions table
        Schema::drop('questions');
    }
}
