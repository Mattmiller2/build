<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
  //Allows for the entry of title and the content from the form to hte database.
    protected $fillable = [
      'title',
      'content'
    ];

    /**
     *Links User and Question table
     */
    public function user()
    {
      //One to One Connection
      return $this->belongsTo('App\User');
    }

    /**
     *Links Question and Questionnaire tables
     */
    public function questionnaires()
    {
      //One To Many Connection
      return $this->belongsToMany('App\Questionnaire');
    }
}
