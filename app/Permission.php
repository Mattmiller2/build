<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
  /**
   *Links permissions and roles table
   */
    public function roles() {
      //One to Many relation
      return $this->belongsToMany(Role::class);
    }
}
