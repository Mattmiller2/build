<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Question;

class QuestionController extends Controller
{
   /*
    * Secure the set of pages to the Admin.
    */
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //Demands that user has permission to create a question
      if (Gate::allows('create_question')){
        //returns questions view with all data from the table.
        $questions = Question::all();
        return view('/admin/questions', ['questions' => $questions]);
      }
      //If the user doesn't have permission they are returned to the hoem screen.
      return view('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Returns user to question create form.
        return view('admin/questions/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //Stores all inputted data into corresponding table
        $question = Question::create($request->all());

        return redirect('admin/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //Looks for question with the selected value.
        $question = Question::where('id',$id)->first();
      //If question does not exist the user is returned to the questions blade
        if(!$question)
        {
          return redirect('/admin/questions');
        }
        //If question value is found return show blade with details of the specifc question.
        return view('/admin/questions/show')->withQuestion($question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
