<?php

namespace App\Http\Controllers;
use Gate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Response;
use App\Answer;

class ResponseController extends Controller
{
    /*
    * Secure the set of pages to the Admin.
    */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //Demands user has permission to see responses.
      if (Gate::allows('see_responses')){
        //returns blade with all responses to questionnaire.
        $responses = Response::all();
        return view('/admin/responses', ['responses' => $responses]);
      }
      //if user doesn't have the required permissions.
      return view('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //Searches for specific questionnaire selected.
      $response = Response::with('answer')->where('id', $id)->first();

      //If the questionnaire doesn't exist the user is returned to the responses blade
      if(!$response)
      {
        return redirect('/admin/responses');
      }
      //show blade is returned with response data shown.
      return view('/admin/responses/show')->withResponse($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
