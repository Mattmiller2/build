<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Questionnaire;
use App\Answer;
use App\Response;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //Returns all created Questionnaires
        $questionnaires = Questionnaire::all();

        return view('/participants', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //stores data into table and links selected answer
      $responses = Response::create($request->all());
      $responses->answer()->attach($request->input('answer'));

      return redirect('/participants');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //pulls selected questionnaire with linked questions
      $questionnaire = Questionnaire::with('question')->where('id', $id)->first();
      //Pulls answers from answer table into muliple choice questionss
      $answers = Answer::lists('title', 'id');

      //If questionnaire selcted does not exist
      if(!$questionnaire)
      {
        //returns user to participants blade
        return redirect('/participants');
      }
      //Otherwise it returns questionnaire and answers in the show blade.
      return view('/participants/show', compact('answers'))->withQuestionnaire($questionnaire);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
