<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Questionnaire;
use App\Question;


class QuestionnaireController extends Controller
{
    /*
    * Secure the set of pages to the Admin.
    */
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //Demands that the user has permission to create questionnaires
      if (Gate::allows('create_questionnaire')){
        //Pulls all questionnaires from the table
        $questionnaires = Questionnaire::all();
        //returns questionnaires in main blade.
        return view('/admin/questionnaires', ['questionnaires' => $questionnaires]);
      }
      return view('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //Links questions into the creation form to be selected from.
        $questions = Question::lists('content', 'id');
        return view('/admin/questionnaires/create', compact('questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //Stores input information from the form and links any chosen questions.
        $questionnaire = Questionnaire::create($request->all());
        $questionnaire->question()->attach($request->input('question'));

        return redirect('/admin/questionnaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //Looks for questionnaire selected by user in the form.
        $questionnaire = Questionnaire::with('question')->where('id', $id)->first();
      //If the questionnaire does not exist the user is taken back to questionnaires blade
        if(!$questionnaire)
        {
          return redirect('/admin/questionnaires');
        }
      //returns show blade, with data from the selected questionnaire.
        return view('/admin/questionnaires/show')->withQuestionnaire($questionnaire);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
