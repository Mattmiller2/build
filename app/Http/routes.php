<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Open Front end for participants
Route::resource('/participants', 'ParticipantController');

//Implements Authorisation for the website.
Route::group(['middleware' => 'web'], function () {
  Route::auth();

//Shows home blade
  Route::get('/home', 'HomeController@index');

  //Shows Response blade
  Route::resource('/admin/responses', 'ResponseController');

  //shows Questions blade
  Route::resource('/admin/questions', 'QuestionController');

  //shows questionnaires blade
  Route::resource('/admin/questionnaires', 'QuestionnaireController');

  //shows answers blade
  Route::resource('/admin/answers', 'AnswerController');
});
