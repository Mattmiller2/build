<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
  /**
   *Links the Answer and Responses table
   */
    public function response()
    {
      //One to Many association
      return $this->belongsToMany('App\Response');
    }
}
