<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
  /**
   *Allows for entry into database, via the form
   */
    protected $fillable = [
      'title'
    ];

    /**
     *Links the Answer and Responses table
     */
    public function answer()
    {
      //One to Many relation
      return $this->belongsToMany('App\Answer');
    }
}
