<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
  //For input of title and answers A to D for Questions 1 to 5
    protected $fillable = [
      'title',
      'Q1_answerA',
      'Q1_answerB',
      'Q1_answerC',
      'Q1_answerD',
      'Q2_answerA',
      'Q2_answerB',
      'Q2_answerC',
      'Q2_answerD',
      'Q3_answerA',
      'Q3_answerB',
      'Q3_answerC',
      'Q3_answerD',
      'Q4_answerA',
      'Q4_answerB',
      'Q4_answerC',
      'Q4_answerD',
      'Q5_answerA',
      'Q5_answerB',
      'Q5_answerC',
      'Q5_answerD'
    ];

    /**
     *Links Question and Questionnaire Tables
     */
    public function question()
    {
      //One to Many relation
      return $this->belongsToMany('App\Question');
    }

    /**
     *Links User and Questionnaire tables
     */
    public function user()
    {
      //One to One Connection
      return $this->belongsTo('App\User');
    }
}
