<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  /**
   *Links the Role and permissions tables
   */
    public function permissions() {
      //One to Many Relation
      return $this->belongsToMany(Permission::class);
    }

    /**
     *Assigns permissions to Roles.
     */
    public function givePermissionTo(Permission $permission) {
      return $this->permission()->sync($permission);
    }
}
