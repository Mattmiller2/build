<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *Links the User and Roles table
     */
    public function roles() {
      //One to Many Relation
      return $this->belongsToMany(Role::class);
    }

    /**
     * Checks that the user has a valid role attached to them.
     */
    public function hasRole($role) {
        if (is_string($role)){
          return $this->roles->contains('name', $role);
        }
        //If the role doesnt exist thereis a warning.
        return !! $role->intersect($this->roles)->count();
      }

      /**
       *Allows for roles to be assigned to users
       */
    public function assignRole($role) {
      return $this->roles()->sync(
        Role::whereName($role)->firstOrFail()
      );
    }
}
